# magritt.py

Pipes for python in magrittr style

R has the wonderful library magrittr which lets you use pipes in the language.
Although there are already several packages in python which provide pipes. Here we present another implementation.

The package overloads the >> and << operator for forward or backward pipes and the % operator for decoration of functions. 
The % operator is used, because the @ operator is not available in python2.